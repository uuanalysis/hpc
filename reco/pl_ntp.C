/** Macro to plot MCTruth and RECO after performing the PndLLbarAnaTask
 *  Analysis. The MCTruth are stored in ntpMCTruth and the decay particle
 *  into ther corresponding ntuples e.g. proton > ntpProton.root, etc.
 *
 *  Same as pl_fStates.C except it takes MCTruth after analysis (ntpBestPbarP.C)
 */
 
#include <vector>
#include <iostream>
#include <fstream>
#include <cmath>

#include "DrawOpt.C"

void pl_ntp() {

    //*** Open Histogram Root File
    TFile *f = TFile::Open("ntpBestPbarP.root");  // Reconstructed Events
    if (!f) return;
    f->ls();


    //-------------------------------------------------------------------------
    //                               Histograms
    //-------------------------------------------------------------------------
    TString hist_rec = "", hist_mc = "", cut = "all_";
    
    //----- Kinematics: MC (f) & REC (f)
    
    //hist_rec = "hpbar_MomTht"; hist_mc = "hpbar_McMomTht";
    //hist_rec = "hp_MomTht"; hist_mc = "hp_McMomTht";
    hist_rec = "hpiplus_MomTht"; hist_mc = "hpiplus_McMomTht";
    //hist_rec = "hpiminus_MomTht"; hist_mc = "hpiminus_McMomTht";
    
    //hist_rec = "hlam0bar_MomTht"; hist_mc = "hlam0bar_McMomThtt";
    //hist_rec = "hlam0_MomTht"; hist_mc = "hlam0_McMomTht";
    
    //hist_rec = "hlam0bar_PzPt"; hist_mc = "hlam0bar_McPzPt";
    //hist_rec = "hlam0_PzPt"; hist_mc = "hlam0_McPzPt";
    
    hist_rec = "MomThtAll"; hist_mc = "McMomThtAll";
    hist_rec = "PzPtAll"; hist_mc = "McPzPtAll";
    
    // ----- Scatter Plots
    
    //hist_rec = "h_dvxy_lam0"; hist_mc = "h_dvxy_lam0";
    //hist_rec = "h_dvxy_lam0bar"; hist_mc = "h_dvxy_lam0bar";
    //hist_rec = "h_m_llbar"; hist_mc = "h_m_llbar";
    
    
    //----- Pre-selection
    //hist_rec = "hlam0_m";
    //hist_rec = "hlam0bar_m";
    //hist_rec = "hpbarp_m";
    //hist_rec = "hpbarp_chi2";
    //hist_rec = "hpbarp_prob";
    //hist_rec = "h_dvzsum_llbar";
    //hist_rec = "h_m_llbar";  // 2D

    //----- Final Selection
    //hist_rec = "hlam0_m_cut3";
    //hist_rec = "hlam0bar_m_cut3";
    //hist_rec = "hlam0_thtcm";
    //hist_rec = "hlam0bar_thtcm";
    
    
    //----- Clone Histograms
    TH1D *h1 = (TH1D*)(f->Get(hist_rec)); TH1D *h1c = (TH1D*)h1->Clone("htemp");  //ntpBestPbarP
    TH1D *h2 = (TH1D*)(f->Get(hist_mc)); TH1D *h2c = (TH1D*)h2->Clone("htemp");  //ntpBestPbarP

    
    //Mean & STD of Mass
    Double_t mm = h1c->GetMean(); 
    Double_t rms= h1c->GetRMS();
    
    std::cout <<"Mean: " << mm << " RMS: " << rms << std::endl;
    
    //-------------------------------------------------------------------------
    //                          Arrows, Lines, Legends, etc.
    //-------------------------------------------------------------------------

    //*** TLine::TLine(Double_t x1, Double_t y1, Double_t x2, Double_t y2)
    TLine *l1 = new TLine(1.101,0.,1.101,53000);  // mass
    //TLine *l1 = new TLine(100,0,100,22250);  // chi2
    //TLine *l1 = new TLine(2.0,0,2.0,66000);  // z_lam0 + z_lam0bar
    l1->SetLineColor(kRed);
    l1->SetLineWidth(1.0);

    //*** TLine::TLine(Double_t x1, Double_t y1, Double_t x2, Double_t y2)
    TLine *l2 = new TLine(1.130,0.,1.130,53000);  // mass
    l2->SetLineColor(kRed);
    l2->SetLineWidth(1.0);

    //**** TLegend::TLegend(Double_t x1, Double_t y1, Double_t x2, Double_t y2)
    TLegend *leg = new TLegend(0.65,0.81,0.89,0.89);
    leg->AddEntry(h1c,"Signal","l");  // l or f
    leg->AddEntry(h2c,"Background","l");  // l or f
    
    //-------------------------------------------------------------------------
    //                               Text, Latex, etc
    //-------------------------------------------------------------------------

    //*** TLatex::TLatex(Double_t x, Double_t y, const char *text)
    TLatex tex;
    tex.SetTextAlign(13);
    tex.SetTextColor(kGray+1);
    tex.SetTextFont(42);
    tex.SetTextSize(0.08);

    //*** TText::TText (Double_t x, Double_t y, const char *text)
    TText *t1 = new TText(1.3,57,"#bar{p}");  // pbar
    //TText *t1 = new TText(1.3,57,"p");  // p
    //TText *t1 = new TText(1.3,57,"p");  // piplus
    //TText *t1 = new TText(1.3,57,"p");  // piminus
    t1->SetTextAlign(13);
    t1->SetTextColor(kGray+1);
    t1->SetTextFont(42);
    t1->SetTextSize(0.05);
    
    //-------------------------------------------------------------------------
    //                               Stacking
    //-------------------------------------------------------------------------

    // Stack TH1/TH2 Histograms (better for plotting). Remember, individual
    // histogram labels will be lost, so do set as follows, after hs->Draw():

    // hs->GetXaxis()->SetTitle("the X axis");
    // hs->GetYaxis()->SetTitle("the Y axis"); c1->Modified();

    THStack *hs = new THStack("hs","THStack");
    
    //-------------------------------------------------------------------------
    //                               Styling
    //-------------------------------------------------------------------------

    // Common Styling for All
    DrawOpt(h1c); DrawOpt(h2c);
    
    //Indiviudal Styling: Signal
    //h1c->SetStats(kTRUE);
    //h1c->SetLineColor(kBlue);
    //h1c->SetLineWidth(1);
    hs->Add(h1c);                                     // stacking
    
    //h2c->SetStats(kTRUE);
    //h2c->SetLineColor(kBlack);
    //h2c->SetLineWidth(1);
    hs->Add(h2c);                                     // stacking
    
    //*************************************************************************
    //                               TCanvas
    //*************************************************************************
    
    /*
    TCanvas *c1 = new TCanvas("c1","MC vs. REC Histograms",0,0,1200,500);
    c1->Divide(2,1);
    c1->cd(1); gPad->SetLogy(false); h1c->Draw("colz");
    c1->cd(2); gPad->SetLogy(false); h2c->Draw("colz");
    c1->SaveAs((cut+hist_name+"_1.png"));
    c1->Close();
    */
    
    //*************************************************************************
    //                               TCanvas
    //*************************************************************************    
    //TCanvas *c2 = new TCanvas("c2","REC",0,0,600,500);
    TCanvas * c2 = new TCanvas("c2","REC");
	c2->SetCanvasSize(1200, 1000);
   	c2->SetWindowSize(1200, 1000);
	c2->SetTopMargin(0.025);
	c2->SetBottomMargin(0.15);
	c2->SetLeftMargin(0.15);
    
    // Draw Hist.
    c2->SetLogy(false); c2->SetLogz(true);
    h1c->SetStats(kFALSE);
    h1c->Draw("colz");
    
    // Draw Latex
    if(hist_rec.Contains("hpbar_"))
        tex.DrawLatex(1.5,59,"#bar{p}");
        
    if(hist_rec.Contains("hp_"))
        tex.DrawLatex(1.5,59,"p");
        
    if(hist_rec.Contains("hpiminus_"))
        tex.DrawLatex(1.5,175,"#pi^{-}");
        
    if(hist_rec.Contains("hpiplus_"))
        tex.DrawLatex(1.5,175,"#pi^{+}");
        
    if(hist_rec.Contains("hlam0bar_"))
        tex.DrawLatex(1.5,0.49,"#bar{#Lambda}");
        
    if(hist_rec.Contains("hlam0_"))
        tex.DrawLatex(1.5,0.49,"#Lambda");
    
    c2->Draw();
    c2->SaveAs((cut+hist_rec+".png"));
    //c2->Close();
    
    
    //*************************************************************************
    //                               TCanvas
    //*************************************************************************
    //TCanvas *c3 = new TCanvas("c3","MCTruth",0,0,600,500);
    TCanvas * c3 = new TCanvas("c3","MCTruth");
	c3->SetCanvasSize(1200, 1000);
   	c3->SetWindowSize(1200, 1000);
	c3->SetTopMargin(0.025);
	c3->SetBottomMargin(0.15);
	c3->SetLeftMargin(0.15);
	
    // Draw Hist.
    c3->SetLogy(false); c3->SetLogz(true);
    h2c->SetStats(kFALSE);
    h2c->Draw("COLZ");
    
    // Draw Latex
    if(hist_mc.Contains("hpbar_"))
        tex.DrawLatex(1.5,59,"#bar{p}");
        
    if(hist_mc.Contains("hp_"))
        tex.DrawLatex(1.5,59,"p");
        
    if(hist_mc.Contains("hpiminus_"))
        tex.DrawLatex(1.5,175,"#pi^{-}");
        
    if(hist_mc.Contains("hpiplus_"))
        tex.DrawLatex(1.5,175,"#pi^{+}");
        
    if(hist_mc.Contains("hlam0bar_"))
        tex.DrawLatex(1.5,0.49,"#bar{#Lambda}");
        
    if(hist_mc.Contains("hlam0_"))
        tex.DrawLatex(1.5,0.49,"#Lambda");
    
    c3->Draw();
    c3->SaveAs((cut+hist_mc+".png"));
    c3->Close();

}//end-macro
