#include <vector>
#include <iostream>
#include <fstream>
#include <cmath>

using std::cout;
using std::endl;

bool CheckFile(TString fn);
void analyse_ptheta(TString prefix="fwp1", int from=1, int to=11) {
    
    TString path = "./bfwp/";
    TString input = "";
    TString parFile  = TString::Format(path+"%s_%d_par.root", prefix.Data(), from);
    TString simFile  = TString::Format(path+"%s_%d_sim.root", prefix.Data(), from);
    TString digiFile = TString::Format(path+"%s_%d_digi.root", prefix.Data(), from);
    TString recoFile = TString::Format(path+"%s_%d_reco.root", prefix.Data(), from);
    TString outFile  = "out.root";  // SetOutputFile() of the FairRunAna
    
    /* Jenny
    TString parAsciiFile = "all.par";
	TString input = "ll";
	TString simFile = "sim";
	TString parFile = "par";
	TString digiFile ="digi";
	TString recoFile = "recoideal";
	TString prefix("../data/evtcomplete");
	TString outFile = "out"; //of the FairRunAna
	*/
	
	TFile *out = TFile::Open("analyse_momtheta.root", "RECREATE"); //output for the hsitograms

	// ******************* Define variables ***********************************************
	int Ev_start = 0;
	int Ev_end = 0;     // take 0 for all events

	int hits_STT=0; // total number of STT hits
	int hits_STT_per_track=0; // number of STT hits per track

	int reco=0; // set to one if a track is reconstructible
	int reco_events=0; // number of reconstructable events
	int reco_tracks=0; // number of reconstructable tracks

	int num_protons=0; // number of reconstructed proton tracks
	int num_anti_protons=0; // number of reconstructed anti proton tracks
	int num_pi_plus=0; // number of reconstructed pi plus tracks
	int num_pi_minus=0; // number of reconstructed pi minus tracks

	int proton_track=0; // if a proton track is found in the track array this is set to one
	int pi_minus_track=0; // if a pi minus is found in the track array this is set to one
	int antiProton_track=0; // if an anti-proton track is found in the track array this is set to one
	int pi_plus_track=0; // if a pi plus is found in the track array this is set to one

	int antiProton=0; // if an  anti proton track is reconstructed this is set to one for the event
	int pi_plus=0; // if a pi plus track is reconstructed this is set to one for the event
	int proton=0; // if a proton track is reconstructed this is set to one for the event
	int pi_minus=0; // if a pi minus track is reconstructed this is set to one for the event

	int reco_tracks_per_event=0; // number of reconstructible tracks per event
	int reco_anti_lambda=0; // number of reconstructible anti-lambda
	int reco_lambda=0; // number of reconstructinle lambda
	int reco_lambda_anti_lambda=0; // number of reconstructible lambda and anti-lambda in the same event

	double rMomAll;
	double zMomAll;
	double rMomGenCreatedAll;
	double zMomGenCreatedAll;
	double rMomLambda;
	double zMomLambda;
	double rMomAntiLambda;
	double zMomAntiLambda;
	double rMomGenCrProton;
	double zMomGenCrProton;
	double rMomGenCrAntiProton;
	double zMomGenCrAntiProton;
	double rMomGenCrPiMinus;
	double zMomGenCrPiMinus;
	double rMomGenCrPiPlus;
	double zMomGenCrPiPlus;

	double rMomPiMinus;
	double zMomPiMinus;
	double rMomProton;
	double zMomProton;
	double rMomAntiProton;
	double zMomAntiProton;
	double rMomPiPlus;
	double zMomPiPlus;

    // *** Timer
    TStopwatch timer;
    timer.Start();

    // ************************************************************************
    //                 Access TObject using FairRunAna, FairLinks
    // ************************************************************************
    
    // *** PndMasterRunAna (Jenny)
    // FIXME: see how multiple files can be added
    
    /*
    PndMasterRunAna *fRun= new PndMasterRunAna();
	fRun->SetInput(input);
	fRun->AddFriend(recoFile);
	fRun->AddFriend(simFile);
	fRun->AddFriend(digiFile);
	fRun->SetOutput(outFile);
	fRun->SetParamAsciiFile(parAsciiFile);
	fRun->Setup(prefix);
	fRun->SetUseFairLinks(kTRUE);
	fRun->Init();
    */
    
    // ------------------------------------------------------------------------
	//				   Access TObject using FairRunAna, FairLinks
	// ------------------------------------------------------------------------
	
    // *** FairRunAna
    FairLogger::GetLogger()->SetLogToFile(kFALSE);
    FairRunAna *fRun = new FairRunAna();
    fRun->SetUseFairLinks(kTRUE);

    // --------------------------------------------------------
    // ........Using FairFileSource & FairRootFileSink.........
    
    // (1) Add First File to FairFileSource
    FairFileSource *fSrc = new FairFileSource(recoFile);
    
    
    // (2) Add Rest Files to FairFileSource
    for (int i=from+1; i<=to; ++i) {
        TString recoFile = TString::Format(path+"%s_%d_reco.root", prefix.Data(), from, i);
        if (CheckFile(recoFile)) fSrc->AddFile(recoFile);
    }
    
    fSrc->AddFriend(simFile);
    for (int i=from+1; i<=to; ++i) {
        TString simFile  = TString::Format(path+"%s_%d_sim.root", prefix.Data(), from, i);
        if (CheckFile(simFile)) fSrc->AddFriend(simFile);
    }
    
    fSrc->AddFriend(digiFile);
    for (int i=from+1; i<=to; ++i) {
        TString digiFile = TString::Format(path+"%s_%d_digi.root", prefix.Data(), from, i);
        if (CheckFile(digiFile)) fSrc->AddFriend(digiFile);
    }
    
    // Add Source
    fRun->SetSource(fSrc);

    // Add Output File to FairRootFileSink
    FairRootFileSink *fSink = new FairRootFileSink(outFile);
    fRun->SetSink(fSink);
    
	// FairRuntimeDb
	FairRuntimeDb* rtdb = fRun->GetRuntimeDb();
	FairParRootFileIo* parInput1 = new FairParRootFileIo();
	parInput1->open(parFile.Data());
	
	// FairParAsciiFileIo
	FairParAsciiFileIo* parIo1 = new FairParAsciiFileIo();
	TString allDigiFile = gSystem->Getenv("VMCWORKDIR"); 
	allDigiFile += "/macro/params/all.par";
	parIo1->open(allDigiFile.Data(), "in");
	rtdb->setFirstInput(parInput1);
	rtdb->setSecondInput(parIo1);

	// FairRunAna::Init()
	fRun->Init();
    
	// FairRootManager::Instance()
	FairRootManager* ioman = FairRootManager::Instance();
	
	
	// ........Using FairFileSource & FairRootFileSink.........
    // --------------------------------------------------------
    
	
	// ------------------------------------------------------------------------
    //                                  TClonesArrays
    // ------------------------------------------------------------------------
    
    // TClonesArray
	TClonesArray* mcTrackArray = (TClonesArray*) ioman->GetObject("MCTrack"); // if not "initialized" here it may cause errors
	TClonesArray* SttMvdGemTrackArray = (TClonesArray*)ioman->GetObject("BarrelTrack");
	TClonesArray* TrackCandArray = (TClonesArray*)ioman->GetObject("BarrelTrackCand");
	TClonesArray* sttHitArray = (TClonesArray*) ioman->GetObject("STTHit");  // should be loaded for FairLinks
	//TClonesArray* sciTHitArray = (TClonesArray*) ioman->GetObject("SciTHit");
	//TClonesArray* mvdHitsPixelArray = (TClonesArray*) ioman->GetObject("MVDHitsPixel");
	//TClonesArray* mvdHitsStripArray = (TClonesArray*) ioman->GetObject("MVDHitsStrip");

	PndMCTrack *mcTrack;
	PndTrack *sttMvdGemTrack;
	PndTrackCand trackCand;
	PndSciTHit *sciTHit;
	PndSttHit *sttHit;
	PndSdsDigiPixel *mvdHitsPixel;
	PndSdsDigiStrip *mvdHitsStrip;

	FairMultiLinkedData links;
	FairMultiLinkedData link;
	FairMultiLinkedData linksMCTrack;
	FairMultiLinkedData linksMvdPixel;
	FairMultiLinkedData linksMvdStrip;
	FairMultiLinkedData linksSTT;
	FairMultiLinkedData linksSciT;

	///////////////////////////////////////////////  STT /////////////////////////////////////////////////
	TH1F *sttHitsProton = new TH1F("sttHitsProton","Protons, 1.642 GeV",110,0,110);
	sttHitsProton->GetXaxis()->SetTitle("Number of STT hits");
	sttHitsProton->GetYaxis()->SetTitle("Number of tracks");

	TH1F *sttHitsPiMinus = new TH1F("sttHitsPiMinus","Pi minus, 1.642 GeV",110,0,110);
	sttHitsPiMinus->GetXaxis()->SetTitle("Number of STT hits");
	sttHitsPiMinus->GetYaxis()->SetTitle("Number of tracks");

	TH1F *sttHitsAntiProton = new TH1F("sttHitsAntiProton","Anti-Protons, 1.642 GeV",110,0,110);
	sttHitsAntiProton->GetXaxis()->SetTitle("Number of STT hits");
	sttHitsAntiProton->GetYaxis()->SetTitle("Number of tracks");

	TH1F *sttHitsPiPlus = new TH1F("sttHitsPiPlus","Pi plus, 1.642 GeV",110,0,110);
	sttHitsPiPlus->GetXaxis()->SetTitle("Number of STT hits");
	sttHitsPiPlus->GetYaxis()->SetTitle("Number of tracks");

	//////////////////////////////////// Momentum distributions ////////////////////////////////////////////

	TH2F *momentumDistrPiMinus= new TH2F("momentumDistrPiMinus","Pi minus, 1.642 GeV",470,-0.1,4.6,460,0,4.6);
	momentumDistrPiMinus->GetXaxis()->SetTitle("z-momentum [GeV]");
	momentumDistrPiMinus->GetYaxis()->SetTitle("R-momentum [GeV]");

	TH1F *trackPtPiMinus= new TH1F("trackPtPiMinus","Pi minus, 1.642 GeV",170,-0.10,1.7);
	trackPtPiMinus->GetXaxis()->SetTitle("Transverse Momentum [GeV]");
	trackPtPiMinus->GetYaxis()->SetTitle("Number of Tracks");


	TH2F *momentumDistrProton= new TH2F("momentumDistrProton","Proton, 1.642 GeV",470,-0.1,4.6,460,0,4.6);
	momentumDistrProton->GetXaxis()->SetTitle("z-momentum [GeV]");
	momentumDistrProton->GetYaxis()->SetTitle("R-momentum [GeV]");

	TH1F *trackPtProton= new TH1F("trackPtProton","Proton, 1.642 GeV",170,0,1.7);
	trackPtProton->GetXaxis()->SetTitle("Transverse Momentum [GeV]");
	trackPtProton->GetYaxis()->SetTitle("Number of Tracks");

	TH2F *momentumDistrPiPlus= new TH2F("momentumDistrPiPlus","Pi plus, 1.642 GeV",470,-0.1,4.6,460,0,4.6);
	momentumDistrPiPlus->GetXaxis()->SetTitle("z-momentum [GeV]");
	momentumDistrPiPlus->GetYaxis()->SetTitle("R-momentum");

	TH1F *trackPtPiPlus= new TH1F("trackPtPiPlus","Pi plus, 1.642 GeV",170,0,1.7);
	trackPtPiPlus->GetXaxis()->SetTitle("Transverse Momentum [GeV]");
	trackPtPiPlus->GetYaxis()->SetTitle("Number of Tracks");

	TH2F *momentumDistrAntiProton= new TH2F("momentumDistrAntiProton","Anti proton, 1.642 GeV",470,-0.1,4.6,460,0,4.6);
	momentumDistrAntiProton->GetXaxis()->SetTitle("z-momentum [GeV]");
	momentumDistrAntiProton->GetYaxis()->SetTitle("R-momentum [GeV]");

	TH1F *trackPtAntiProton= new TH1F("trackPtAntiProton","Anti proton, 1.642 GeV",170,0,1.7);
	trackPtAntiProton->GetXaxis()->SetTitle("Transverse Momentum [GeV]");
	trackPtAntiProton->GetYaxis()->SetTitle("Number of Tracks");

	TH2F *momentumDistrAll= new TH2F("momentumDistrAll","All particles, 1.642 GeV",1400,-7,7,350,0,3.5);
	momentumDistrAll->GetXaxis()->SetTitle("z-momentum [GeV]");
	momentumDistrAll->GetYaxis()->SetTitle("R-momentum [GeV]");

	TH2F *momentumDistrGenCreatedAll= new TH2F("momentumDistrGenCreatedAll","All generator created particles, 1.642 GeV",150,-0.1,1.4,50,0,0.5);
	momentumDistrGenCreatedAll->GetXaxis()->SetTitle("z-momentum [GeV]");
	momentumDistrGenCreatedAll->GetYaxis()->SetTitle("R-momentum [GeV]");

	TH2F *momentumDistrLambda= new TH2F("momentumDistrLambda","Lambda, 1.642 GeV",470,-0.1,4.6,460,0,4.6);
	momentumDistrLambda->GetXaxis()->SetTitle("z-momentum [GeV]");
	momentumDistrLambda->GetYaxis()->SetTitle("R-momentum [GeV]");

	TH2F *momentumDistrAntiLambda= new TH2F("momentumDistrAntiLambda","Anti lambda, 1.642 GeV",470,-0.1,4.6,460,0,4.6);
	momentumDistrAntiLambda->GetXaxis()->SetTitle("z-momentum [GeV]");
	momentumDistrAntiLambda->GetYaxis()->SetTitle("R-momentum [GeV]");

	TH2F *momDistrGenCreatedProtons= new TH2F("momDistrGenCreatedProtons","Proton, 1.642 GeV",470,-0.1,4.6,460,0,4.6);
	momDistrGenCreatedProtons->GetXaxis()->SetTitle("z-momentum [GeV]");
	momDistrGenCreatedProtons->GetYaxis()->SetTitle("R-momentum [GeV]");

	TH2F *momDistrGenCreatedAntiProtons= new TH2F("momDistrGenCreatedAntiProtons","Anti Proton, 1.642 GeV",470,-0.1,4.6,460,0,4.6);
	momDistrGenCreatedAntiProtons->GetXaxis()->SetTitle("z-momentum [GeV]");
	momDistrGenCreatedAntiProtons->GetYaxis()->SetTitle("R-momentum [GeV]");

	TH2F *momDistrGenCreatedPiPlus= new TH2F("momDistrGenCreatedPiPlus","Pi plus, 1.642 GeV",470,-0.1,4.6,460,0,4.6);
	momDistrGenCreatedPiPlus->GetXaxis()->SetTitle("z-momentum [GeV]");
	momDistrGenCreatedPiPlus->GetYaxis()->SetTitle("R-momentum [GeV]");

	TH2F *momDistrGenCreatedPiMinus= new TH2F("momDistrGenCreatedPiMinus","Pi minus, 1.642 GeV",470,-0.1,4.6,460,0,4.6);
	momDistrGenCreatedPiMinus->GetXaxis()->SetTitle("z-momentum [GeV]");
	momDistrGenCreatedPiMinus->GetYaxis()->SetTitle("R-momentum [GeV]");

	///////////////////////////////////////////////////////////////
	// Momentum vs angle plots
	TH2F *momVsPolarAngleGenerated= new TH2F("momVsPolarAngleGenerated","",160,-0.1,1.5,180,0,180);
        momVsPolarAngleGenerated->GetXaxis()->SetTitle("|p| / 0.01 GeV/c");
        momVsPolarAngleGenerated->GetYaxis()->SetTitle("Polar angle, #theta / degrees");

	TH2F *momVsPolarAngleReco= new TH2F("momVsPolarAngleReco","",160,-0.1,1.5,180,0,180);
        momVsPolarAngleReco->GetXaxis()->SetTitle("|p| / 0.01 GeV/c");
        momVsPolarAngleReco->GetYaxis()->SetTitle("Polar angle, #theta / degrees");
	
	///////////////////////////////////////////////////////////////

	if (Ev_end == 0)
		Ev_end = Int_t((ioman->GetInChain())->GetEntries());
	cout << " EndEvent = " << Ev_end << endl;

	for (int i_Event = Ev_start; i_Event < Ev_end; i_Event++) { // ------- Loop over digitized events
		ioman->ReadEvent(i_Event);
		cout << "  " << endl;
		cout << "event: " << i_Event << endl;

		//******************* inside an event ********************

		// Loop over MCtrack array in order to plot the momentum distribution for different quantities
		for (Int_t i_Array = 0; i_Array < mcTrackArray ->GetEntries(); i_Array++) { //loop over MCtrack array
			mcTrack = (PndMCTrack *) mcTrackArray ->At(i_Array);

			rMomAll=mcTrack->GetPt();
			zMomAll=mcTrack->GetMomentum().Pz();
			momentumDistrAll->Fill(zMomAll,rMomAll);

			///// Filling the angle vs. momentum histogram
			//momVsPolarAngleGenerated->Fill(mcTrack->GetMomentum().Mag(),mcTrack->GetMomentum().Theta()*(180/3.14));
			  
			if (mcTrack->IsGeneratorCreated()){

				rMomGenCreatedAll=mcTrack->GetPt();
				zMomGenCreatedAll=mcTrack->GetMomentum().Pz();
				momentumDistrGenCreatedAll->Fill(zMomGenCreatedAll,rMomGenCreatedAll);

				momVsPolarAngleGenerated->Fill(mcTrack->GetMomentum().Mag(),mcTrack->GetMomentum().Theta()*(180/3.14));

				if (mcTrack->GetPdgCode()==3122){
					rMomLambda=mcTrack->GetPt();
					zMomLambda=mcTrack->GetMomentum().Pz();
					momentumDistrLambda->Fill(zMomLambda,rMomLambda);
				}

				if(mcTrack->GetPdgCode()==-3122){
					rMomAntiLambda=mcTrack->GetPt();
					zMomAntiLambda=mcTrack->GetMomentum().Pz();
					momentumDistrAntiLambda->Fill(zMomAntiLambda,rMomAntiLambda);
				}

				if (mcTrack->GetPdgCode()==2212){
					rMomGenCrProton=mcTrack->GetPt();
					zMomGenCrProton=mcTrack->GetMomentum().Pz();
					momDistrGenCreatedProtons->Fill(zMomGenCrProton,rMomGenCrProton);
				}

				if(mcTrack->GetPdgCode()==-2212){
					rMomGenCrAntiProton=mcTrack->GetPt();
					zMomGenCrAntiProton=mcTrack->GetMomentum().Pz();
					momDistrGenCreatedAntiProtons->Fill(zMomGenCrAntiProton,rMomGenCrAntiProton);
				}

				if (mcTrack->GetPdgCode()==-211){
					rMomGenCrPiMinus=mcTrack->GetPt();
					zMomGenCrPiMinus=mcTrack->GetMomentum().Pz();
					momDistrGenCreatedPiMinus->Fill(zMomGenCrPiMinus,rMomGenCrPiMinus);
				}

				if(mcTrack->GetPdgCode()==211){
					rMomGenCrPiPlus=mcTrack->GetPt();
					zMomGenCrPiPlus=mcTrack->GetMomentum().Pz();
					momDistrGenCreatedPiPlus->Fill(zMomGenCrPiPlus,rMomGenCrPiPlus);
				}
			}

		} // end MCtrack array

		// ***************** set to zero for each event ************************************************************
		reco_tracks_per_event=0;
		proton_track=0;
		pi_minus_track=0;
		antiProton_track=0;
		pi_plus_track=0;
		// *********************************************************************************************************

		int num_tracks=SttMvdGemTrackArray->GetEntries();
		for (Int_t i_Array = 0; i_Array < SttMvdGemTrackArray ->GetEntries(); i_Array++) { //loop over trackarray

			// ************** set to zero for each track ***********************************************************
			proton=0;
			pi_minus=0;
			antiProton=0;
			pi_plus=0;

			hits_STT_per_track=0;
			reco=0;
			// ****************************************************************************************************

			sttMvdGemTrack = (PndTrack *) SttMvdGemTrackArray ->At(i_Array);

			mcTrack = NULL;
			links = sttMvdGemTrack->GetLinksWithType(ioman->GetBranchId("MCTrack")); // create the links between the track and the MCTrack
			if (links.GetNLinks()>0){
				for (Int_t i=0; i<links.GetNLinks(); i++){

					if(links.GetLink(i).GetIndex()==sttMvdGemTrack->GetTrackCand().getMcTrackId()){

						mcTrack= (PndMCTrack *) ioman->GetCloneOfLinkData(links.GetLink(i));
						//if(mcTrack->GetStartVertex().Z()<100 && mcTrack->GetStartVertex().Perp()<42){
							//if(mcTrack->GetStartVertex().Theta()*180/3.14>10 && mcTrack->GetStartVertex().Theta()*180/3.14<140){

						if (mcTrack->IsGeneratorLast()) {

							// Check the number of STT hits
							linksSTT = sttMvdGemTrack->GetLinksWithType(ioman->GetBranchId("STTHit"));
							if (linksSTT.GetNLinks()>0){
								for (Int_t n=0; n<linksSTT.GetNLinks(); n++){ // loop over the STT hits
									sttHit= (PndSttHit *) ioman->GetCloneOfLinkData(linksSTT.GetLink(n)); // get the object the link is pointing to
									linksMCTrack= sttHit->GetLinksWithType(ioman->GetBranchId("MCTrack"));
									if (linksMCTrack.GetNLinks()>0){ // check if there are links to a MC track for the hit
										mcTrack= (PndMCTrack *) ioman->GetCloneOfLinkData(linksMCTrack.GetLink(0));
										if (mcTrack->IsGeneratorCreated()) {
											hits_STT++;
											hits_STT_per_track++;
										}
									}
								}
							} // STT end


							int j=0; // minimum number of hits in the stt for a track to be reconstructable

							if(hits_STT_per_track>=j){

								reco=1;
								reco_tracks++;
								momVsPolarAngleReco->Fill(mcTrack->GetMomentum().Mag(),mcTrack->GetMomentum().Theta()*(180/3.14));

								if(sttMvdGemTrack->GetPidHypo()==-211){
									num_pi_minus++;
									pi_minus=1;
									sttHitsPiMinus->Fill(sttHitsPiMinus->GetBin(hits_STT_per_track));
									reco_tracks_per_event++;

									rMomPiMinus=mcTrack->GetPt();
									zMomPiMinus=mcTrack->GetMomentum().Pz();

									momentumDistrPiMinus->Fill(zMomPiMinus,rMomPiMinus);
									trackPtPiMinus->Fill(rMomPiMinus);
								}

								if (sttMvdGemTrack->GetPidHypo()==2212){
									num_protons++;
									proton=1;
									sttHitsProton->Fill(sttHitsProton->GetBin(hits_STT_per_track));
									reco_tracks_per_event++;

									rMomProton=mcTrack->GetPt();
									zMomProton=mcTrack->GetMomentum().Pz();

									momentumDistrProton->Fill(zMomProton,rMomProton);
									trackPtProton->Fill(rMomProton);
								}

								if (sttMvdGemTrack->GetPidHypo()==-2212){
									num_anti_protons++;
									antiProton=1;
									sttHitsAntiProton->Fill(sttHitsAntiProton->GetBin(hits_STT_per_track));
									reco_tracks_per_event++;

									rMomAntiProton=mcTrack->GetPt();
									zMomAntiProton=mcTrack->GetMomentum().Pz();

									momentumDistrAntiProton->Fill(zMomAntiProton,rMomAntiProton);
									trackPtAntiProton->Fill(rMomAntiProton);
								}

								if(sttMvdGemTrack->GetPidHypo()==211){
									num_pi_plus++;
									pi_plus=1;
									sttHitsPiPlus->Fill(sttHitsPiPlus->GetBin(hits_STT_per_track));
									reco_tracks_per_event++;

									rMomPiPlus=mcTrack->GetPt();
									zMomPiPlus=mcTrack->GetMomentum().Pz();

									momentumDistrPiPlus->Fill(zMomPiPlus,rMomPiPlus);
									trackPtPiPlus->Fill(rMomPiPlus);
								}

							} // hits stt per track end

						} // if loop over MC tracks linked to one reco track end generator created end

						//}// if MCTrack starts within STT
						//} // if theta within a good range

					} // if index of link==index of rec track
				} // loop over MC tracks linked to one reco track end

			} // if Nlinks to mc track from reco track>0 end

			if (proton==1){
				proton_track=1;
			}
			if (pi_minus==1){
				pi_minus_track=1;
			}
			if (antiProton==1){
				antiProton_track=1;
			}
			if (pi_plus==1){
				pi_plus_track=1;
			}

		} // end track array

		if (antiProton_track==1 && pi_plus_track==1){ // check if anti-lambda was reconstructed
			reco_anti_lambda++;
			if (proton_track==1 && pi_minus_track==1){ // check if both lambda and anti-lambda was reconstructed in the same event
				reco_lambda_anti_lambda++;
			}
		}

		if (proton_track==1 && pi_minus_track==1){ // check if lambda was reconstructed
			reco_lambda++;
			reco_events++;

		}

	} // end event

	out->cd();

	sttHitsProton->Write();
	sttHitsPiMinus->Write();
	sttHitsAntiProton->Write();
	sttHitsPiPlus->Write();

	momentumDistrPiMinus->Write();
	trackPtPiMinus->Write();

	momentumDistrProton->Write();
	trackPtProton->Write();

	momentumDistrPiPlus->Write();
	trackPtPiPlus->Write();

	momentumDistrAntiProton->Write();
	trackPtAntiProton->Write();

	momentumDistrAll->Write();
	momentumDistrGenCreatedAll->Write();

	momentumDistrLambda->Write();
	momentumDistrAntiLambda->Write();

	momDistrGenCreatedProtons->Write();
	momDistrGenCreatedAntiProtons->Write();
	momDistrGenCreatedPiMinus->Write();
	momDistrGenCreatedPiPlus->Write();

	momVsPolarAngleGenerated->Write();
	momVsPolarAngleReco->Write();
	
	// *********** write out you results *****************************************
	cout << "Number of reconstructed tracks: " << reco_tracks << endl;
	cout << "Number of reconstructed events (at least lambdas): " << reco_events << endl;
	cout << " " << endl;
	cout << "From lambda: " << endl;
	cout << "Number of protons: " << num_protons << endl;
	cout << "Number of pi minus: " << num_pi_minus << endl;
	cout << "  " << endl;
	cout << "From anti-lambda: " << endl;
	cout << "Number of anti protons: " << num_anti_protons << endl;
	cout << "Number of pi plus: " << num_pi_plus << endl;
	cout << "  " << endl;
	cout << "Number of reconstructible lambdas: " << reco_lambda << endl;
	cout << "Number of reconstructible anti-lambdas: " << reco_anti_lambda << endl;
	cout << "Number of reconstructible lambdas and anti-lambdas in the same event: " << reco_lambda_anti_lambda << endl;
	out->Save();

	timer.Stop();
	Double_t rtime = timer.RealTime();
	Double_t ctime = timer.CpuTime();
	cout << endl << endl;

	// *********************************************************
	cout << "Macro finished succesfully." << endl;
	cout << "Output file is " << outFile << endl;
	cout << "Parameter file is " << parFile << endl;
	cout << "Real time " << rtime << " s, CPU time " << ctime << " s" << endl;
	cout << endl;
	// -----------
}//end-macro


bool CheckFile(TString fn) {

    bool fileok=true;
    TFile fff(fn); 
    if (fff.IsZombie()) fileok=false;

    TTree *t=(TTree*)fff.Get("pndsim");
    if (t==0x0) fileok=false;

    if (!fileok) std::cout << "Skipping Broken File: '"<< fn << "'" << std::endl;
    return fileok;
}
