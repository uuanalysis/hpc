#include <vector>
#include <iostream>
#include <fstream>
#include <cmath>

using std::cout;
using std::endl;

void plot_recMomTht() {

    TFile *f = new TFile("analyse_momentum_angle.root");

    f->ls();

    //****************************** First histogram ********************************
    TH1F* h1 = (TH1F*)f->Get("momVsPolarAngleReco");

    // Fixing title
    //h1->SetTitle(" ");

    // Fixing the X axis
    h1->GetXaxis()->SetLabelSize(0.055);
    h1->GetXaxis()->SetTitleSize(0.06);
    h1->GetXaxis()->SetTitleOffset(0.99);
    h1->GetXaxis()->CenterTitle();
    h1->GetXaxis()->SetTitle("|P| / GeV/c");

    // Fixing the Y axis
    h1->GetYaxis()->SetLabelSize(0.055);
    h1->GetYaxis()->SetTitleSize(0.06);
    h1->GetYaxis()->SetTitleOffset(0.9);
    h1->GetYaxis()->CenterTitle();

    h1->SetStats(kFALSE);

    // Fixing the layout

    TCanvas * can = new TCanvas();
    can->SetCanvasSize(1500, 1000);
    can->SetWindowSize(1500, 1000);
    can->SetTopMargin(0.025);
    can->SetBottomMargin(0.15);
    can->SetLeftMargin(0.15);
    can->SetLogz();

    h1->Draw("colz");

    // Drawing text
    TLatex latexText(0.4,168,"PANDA MC Simulation");
    latexText.SetTextFont(42);
    latexText.SetTextSize(0.06);
    latexText.DrawClone();
    TLatex latexText(0.4,158,"p_{beam} = 1.642 GeV/c");
    latexText.SetTextFont(62);
    latexText.SetTextSize(0.05);
    latexText.DrawClone();

    TLatex latexText(0.42,80,"STT");
    latexText.SetTextFont(62);
    latexText.SetTextSize(0.05);
    //latexText.SetTextColor(kGray+1);
    latexText.DrawClone();
    TLatex latexText(0.62,80,"MVD");
    latexText.SetTextFont(62);
    latexText.SetTextSize(0.05);
    //latexText.SetTextColor(kGray+1);
    latexText.DrawClone();
    TLatex latexText(0.89,30,"GEM");
    latexText.SetTextFont(62);
    latexText.SetTextSize(0.05);
    //latexText.SetTextColor(kGray+1);
    latexText.DrawClone();
    TLatex latexText(0.82,80,"Barrel TOF");
    latexText.SetTextFont(62);
    latexText.SetTextSize(0.05);
    //latexText.SetTextColor(kGray+1);
    latexText.DrawClone();

    TLine *lineMVD1 = new TLine(-0.1,150,1.5,150);
    lineMVD1->SetLineWidth(2);
    lineMVD1->Draw();

    TLine *lineMVD2 = new TLine(-0.1,3,1.5,3);
    lineMVD2->SetLineWidth(2);
    lineMVD2->Draw();

    TLine *lineSTT1 = new TLine(-0.1,140,1.5,140);
    lineSTT1->SetLineWidth(2);	
    lineSTT1->Draw();

    TLine *lineSTT2 = new TLine(-0.1,10,1.5,10);
    lineSTT2->SetLineWidth(2);	
    lineSTT2->Draw();

    TLine *lineGEM1 = new TLine(-0.1,20,1.5,20);
    lineGEM1->SetLineWidth(2);
    lineGEM1->Draw();

    TLine *lineGEM2 = new TLine(-0.1,3,1.5,3);
    lineGEM2->SetLineWidth(2);
    lineGEM2->Draw();

    TLine *lineBTOF1 = new TLine(-0.1,140,1.5,140);
    lineBTOF1->SetLineWidth(2);
    lineBTOF1->Draw();

    TLine *lineBTOF2 = new TLine(-0.1,22,1.5,22);
    lineBTOF2->SetLineWidth(2);
    lineBTOF2->Draw();

    //TArrow *arrowSTT = new TArrow(xmin,ymin,xmax,ymax);

    TArrow *arrowSTT = new TArrow(0.4,10,0.4,140,0.02,"<|>");
    arrowSTT->SetAngle(40);
    arrowSTT->SetLineWidth(2);
    arrowSTT->Draw();

    TArrow *arrowMVD = new TArrow(0.6,3,0.6,150,0.02,"<|>");
    arrowMVD->SetAngle(40);
    arrowMVD->SetLineWidth(2);
    arrowMVD->Draw();

    TArrow *arrowGEM = new TArrow(1,3,1,20,0.02,"<|>");
    arrowGEM->SetAngle(40);
    arrowGEM->SetLineWidth(2);
    arrowGEM->Draw();

    TArrow *arrowBTOF = new TArrow(0.8,22,0.8,140,0.02,"<|>");
    arrowBTOF->SetAngle(40);
    arrowBTOF->SetLineWidth(2);
    arrowBTOF->Draw();

    can->SaveAs("ptht_rec_1642.png");

    can->Draw();
}
