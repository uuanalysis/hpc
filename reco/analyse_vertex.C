#include <vector>
#include <iostream>
#include <fstream>
#include <cmath>

using std::cout;
using std::endl;

bool CheckFile(TString fn);
void analyse_vertex(TString prefix="fwp1", int from=1, int to=3) {
    
    TString path = "./fwp1/";
    TString input = "";
    TString parFile  = TString::Format(path+"%s_%d_par.root", prefix.Data(), from);
    TString simFile  = TString::Format(path+"%s_%d_sim.root", prefix.Data(), from);
    TString digiFile = TString::Format(path+"%s_%d_digi.root", prefix.Data(), from);
    TString recoFile = TString::Format(path+"%s_%d_reco.root", prefix.Data(), from);
    TString outFile  = "out.root";  // SetOutputFile() of the FairRunAna
	TFile *out = TFile::Open("analyse_vertex.root", "RECREATE"); //output for the hsitograms

	// ******************* Define variables ***********************************************
	int Ev_start = 0;
	int Ev_end = 0;     // take 0 for all events

	int vertex_MVD=0;
	int vertex_STT=0;
	int vertex_within_range_STT=0;
	TVector3 vertexpos;

	// *** Timer
    TStopwatch timer;
    timer.Start();

    // ************************************************************************
    //                 Access TObject using FairRunAna, FairLinks
    // ************************************************************************
    
    // *** PndMasterRunAna (Jenny)
    // FIXME: see how multiple files can be added
    
	/*
    PndMasterRunAna *fRun= new PndMasterRunAna();
	fRun->SetInput(input);
	fRun->AddFriend(recoFile);
	fRun->AddFriend(simFile);
	fRun->AddFriend(digiFile);
	fRun->SetOutput(outFile);
	fRun->SetParamAsciiFile(parAsciiFile);
	fRun->Setup(prefix);
	fRun->SetUseFairLinks(kTRUE);
	fRun->Init();
    */
    
    // ------------------------------------------------------------------------
	//				   Access TObject using FairRunAna, FairLinks
	// ------------------------------------------------------------------------
	
    // *** FairRunAna
    FairLogger::GetLogger()->SetLogToFile(kFALSE);
    FairRunAna *fRun = new FairRunAna();
    fRun->SetUseFairLinks(kTRUE);

    // --------------------------------------------------------
    // ........Using FairFileSource & FairRootFileSink.........
    
    // (1) Add First File to FairFileSource
    FairFileSource *fSrc = new FairFileSource(recoFile);
    
    
    // (2) Add Rest Files to FairFileSource
    for (int i=from+1; i<=to; ++i) {
        TString recoFile = TString::Format(path+"%s_%d_reco.root", prefix.Data(), from, i);
        if (CheckFile(recoFile)) fSrc->AddFile(recoFile);
    }
    
    fSrc->AddFriend(simFile);
    for (int i=from+1; i<=to; ++i) {
        TString simFile  = TString::Format(path+"%s_%d_sim.root", prefix.Data(), from, i);
        if (CheckFile(simFile)) fSrc->AddFriend(simFile);
    }
    
    fSrc->AddFriend(digiFile);
    for (int i=from+1; i<=to; ++i) {
        TString digiFile = TString::Format(path+"%s_%d_digi.root", prefix.Data(), from, i);
        if (CheckFile(digiFile)) fSrc->AddFriend(digiFile);
    }
    
    // Add Source
    fRun->SetSource(fSrc);

    // Add Output File to FairRootFileSink
    FairRootFileSink *fSink = new FairRootFileSink(outFile);
    fRun->SetSink(fSink);
    
	// FairRuntimeDb
	FairRuntimeDb* rtdb = fRun->GetRuntimeDb();
	FairParRootFileIo* parInput1 = new FairParRootFileIo();
	parInput1->open(parFile.Data());
	
	// FairParAsciiFileIo
	FairParAsciiFileIo* parIo1 = new FairParAsciiFileIo();
	TString allDigiFile = gSystem->Getenv("VMCWORKDIR"); 
	allDigiFile += "/macro/params/all.par";
	parIo1->open(allDigiFile.Data(), "in");
	rtdb->setFirstInput(parInput1);
	rtdb->setSecondInput(parIo1);

	// FairRunAna::Init()
	fRun->Init();
    
	// FairRootManager::Instance()
	FairRootManager* ioman = FairRootManager::Instance();
	
	
	// ........Using FairFileSource & FairRootFileSink.........
    // --------------------------------------------------------
    
    
	// ------------------------------------------------------------------------
    //                                  TClonesArrays
    // ------------------------------------------------------------------------

	TClonesArray* mcTrackArray = (TClonesArray*) ioman->GetObject("MCTrack"); // if not "initialized" here it may produces error at the first access
	TClonesArray* SttMvdGemGenTrackArray = (TClonesArray*)ioman->GetObject("IdealTrack");
	TClonesArray* TrackCandArray = (TClonesArray*)ioman->GetObject("IdealTrackCand");

	TClonesArray* sttHitArray = (TClonesArray*) ioman->GetObject("STTHit");

	TClonesArray* mvdPointArray = (TClonesArray*) ioman->GetObject("MVDPoint");
	TClonesArray* mvdHitsPixelArray = (TClonesArray*) ioman->GetObject("MVDHitsPixel");
	TClonesArray* mvdHitsStripArray = (TClonesArray*) ioman->GetObject("MVDHitsStrip");

	PndMCTrack *mcTrack;
	PndTrack *sttMvdGemGenTrack;
	PndTrackCand trackCand;

	PndSttHit *sttHit;
	PndSdsMCPoint *mvdPoint;
	PndSdsDigiPixel *mvdHitsPixel;
	PndSdsDigiStrip *mvdHitsStrip;

	FairMultiLinkedData links;
	FairMultiLinkedData link;
	FairMultiLinkedData linksMCTrack;
	FairMultiLinkedData linksMvdPixel;
	FairMultiLinkedData linksMvdStrip;
	FairMultiLinkedData linksSTT;

	TH2F *posvertex = new TH2F("posvertex","Start vertex MC track, 1.642 GeV",610,-5,300,100,0,50);
	posvertex->GetXaxis()->SetTitle("z-direction [cm]");
	posvertex->GetYaxis()->SetTitle("R-direction [cm]");
	TH2F *posvertex_MVD = new TH2F("posvertex_MVD","Start vertex MC track, 1.642 GeV",610,-5,300,100,0,50);
	posvertex_MVD->GetXaxis()->SetTitle("z-direction [cm]");
	posvertex_MVD->GetYaxis()->SetTitle("R-direction [cm]");
	TH2F *posvertex_STT = new TH2F("posvertex_STT","Start vertex MC track, 1.642 GeV",610,-5,300,100,0,50);
	posvertex_STT->GetXaxis()->SetTitle("z-direction [cm]");
	posvertex_STT->GetYaxis()->SetTitle("R-direction [cm]");

	if (Ev_end == 0)
		Ev_end = Int_t((ioman->GetInChain())->GetEntries());
	cout << " EndEvent = " << Ev_end << endl;

	for (int i_Event = Ev_start; i_Event < Ev_end; i_Event++) { // ------- Loop over digitized events
		ioman->ReadEvent(i_Event);
		cout << "  " << endl;
		cout << "event: " << i_Event << endl;

		//******************* inside an event ********************

		for (Int_t i_Array = 0; i_Array < mcTrackArray ->GetEntries(); i_Array++) { //loop over trackarray
			mcTrack = (PndMCTrack *) mcTrackArray ->At(i_Array);
			if (mcTrack->IsGeneratorLast()) {
				if (mcTrack->GetPdgCode()==2212|mcTrack->GetPdgCode()==-2212){
					vertexpos=mcTrack->GetStartVertex();
					posvertex->Fill(vertexpos.Z(),vertexpos.Perp());

					if (vertexpos.Perp()>=0 && vertexpos.Perp()<13.5){
						if (vertexpos.Z()>-20 && vertexpos.Z()<19){
							vertex_MVD++;
							posvertex_MVD->Fill(vertexpos.Z(),vertexpos.Perp());
						}
					}
					if (vertexpos.Perp()>15 && vertexpos.Perp()<42){
						if (vertexpos.Z()>-55 && vertexpos.Z()<110){
							vertex_STT++;
							posvertex_STT->Fill(vertexpos.Z(),vertexpos.Perp());
						}
					}
					if (vertexpos.Perp()>=0 && vertexpos.Perp()<42){
						if (vertexpos.Z()>-55 && vertexpos.Z()<110){
							vertex_within_range_STT++;
						}
					}
				}
			}
		}

	} // end event

	out->cd();

	posvertex->Write();
	posvertex_MVD->Write();
	posvertex_STT->Write();

	// *********** write out you results *****************************************
	cout << "Total number of events: " << Ev_end << endl;
	cout << "Number of vertices in MVD: " << vertex_MVD << endl;
	cout << "Number of vertices in STT: " << vertex_STT << endl;
	cout << "Number of vertices within range of STT: " << vertex_within_range_STT << endl;
	out->Save();

	timer.Stop();
	Double_t rtime = timer.RealTime();
	Double_t ctime = timer.CpuTime();
	cout << endl << endl;

	// *********************************************************
	cout << "Macro finished succesfully." << endl;
	cout << "Output file is " << outFile << endl;
	cout << "Parameter file is " << parFile << endl;
	cout << "Real time " << rtime << " s, CPU time " << ctime << " s" << endl;
	cout << endl;
	// -----------
}

bool CheckFile(TString fn) {

    bool fileok=true;
    TFile fff(fn); 
    if (fff.IsZombie()) fileok=false;

    TTree *t=(TTree*)fff.Get("pndsim");
    if (t==0x0) fileok=false;

    if (!fileok) std::cout << "Skipping Broken File: '"<< fn << "'" << std::endl;
    return fileok;
}
